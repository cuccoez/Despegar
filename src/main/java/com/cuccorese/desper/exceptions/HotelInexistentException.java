package com.cuccorese.desper.exceptions;

import com.cuccorese.desper.util.errors.ErrorMessages;

/**
 * Excepcion si no se encuentra un hotel para darle LIKE
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public class HotelInexistentException extends ApiException {

    public HotelInexistentException() {
        super(ErrorMessages.NOTFOUND_HOTEL, null);
    }

}
