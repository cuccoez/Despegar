package com.cuccorese.desper.config;

import com.cuccorese.desper.interceptor.HeaderInterceptor;
import java.util.Collections;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Configuracion general de los beans para el contexto de spring.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Configuration
@EnableRedisRepositories
public class BeansConfig {

    @Value("${redis.host}")
    private String hostRedis;

    @Value("${redis.port}")
    private int portRedis;

    /**
     * Rest template para obtener los datos de un servicio RESTFUL.
     *
     * @return
     */
    @Bean
    public RestTemplate restTemplate() {
//        se cambia el request factory del rest template por el de apache para poder trer los datos comprimidos con gzip.
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
                HttpClientBuilder.create().build());

        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        restTemplate.setInterceptors(Collections.singletonList(new HeaderInterceptor()));
        return restTemplate;
    }

    /**
     * Template para redis.
     *
     * @param connectionFactory la facotory para conectar la base de datos.
     * @return el temaplate
     */
    @Bean
    RedisTemplate<?, ?> redisTemplate(RedisConnectionFactory connectionFactory) {

        RedisTemplate<byte[], byte[]> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        //https://github.com/spring-projects/spring-data-examples/tree/master/redis/repositories
        return template;
    }

    /**
     * Conexion a la base de datos.
     *
     * @return la conexion.
     */
    @Bean
    RedisConnectionFactory connectionFactory() {
        JedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory();
        redisConnectionFactory.setHostName(hostRedis);
        redisConnectionFactory.setPort(portRedis);
        return redisConnectionFactory;
    }
}
