package com.cuccorese.desper.interceptor;

import java.io.IOException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * Interceptor para manejar los heads de los request.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public class HeaderInterceptor implements ClientHttpRequestInterceptor {

    private static final String GZIP = "gzip";
    private static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final String X_API_KEY = "X-ApiKey";
    private static final String APIKEY = "c6f65de19e5749acbff0cebe37a43174";

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body,
            ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders headers = request.getHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(X_API_KEY, APIKEY);
        headers.set(ACCEPT_ENCODING, GZIP);
        return execution.execute(request, body);
    }

}
