package com.cuccorese.desper.repository;

import com.cuccorese.desper.domain.Hotel;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 * Repositorio generico para guardar datos y traer datos de hoteles.
 *
 * @author ezequiel.cuccorese
 */
public interface HotelRepository extends CrudRepository<Hotel, String> {

    List<Hotel> findByName(String name);

    List<Hotel> findByCityId(String cityId);

    List<Hotel> findByStars(String stars);

    List<Hotel> findByNameAndStarts(String name, String stars);

}
