package com.cuccorese.desper.repository;

import com.cuccorese.desper.domain.LikeDislikeHotel;
import org.springframework.data.repository.CrudRepository;

/**
 * Repositorio generico para guardar datos y traer datos de los likes de
 * hoteles..
 *
 * @author ezequiel.cuccorese
 */
public interface LikeRepository extends CrudRepository<LikeDislikeHotel, String> {

    LikeDislikeHotel findByHotelId(String hotelId);
}
