package com.cuccorese.desper.domain;

import com.cuccorese.desper.web.response.HotelResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Modelo de datos para wrapear los datos de los servicios de precios.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceWraper {

    private HotelResponse[] items;
}
