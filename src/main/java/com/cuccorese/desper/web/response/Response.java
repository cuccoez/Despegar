/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cuccorese.desper.web.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"currency", "total"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

    private String currency;
    private Float subtotal;
    private Float total;
}
