/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cuccorese.desper.util.menssages;

/**
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public class GeneralMenssages {

    public static final String LOGINFOSNAPSHOT = "log.info.buscandoHoteles";
    public static final String LOGINFOGUARDADOEXITOSO = "log.info.guardadoexitoso";
    public static final String LOGINFOCONTACTANDOALSERVICIO = "log.info.contactandoservicio";
    public static final String LOGERRORCONTACTANDOALSERVICIO = "log.error.contactandoservicio";
}
